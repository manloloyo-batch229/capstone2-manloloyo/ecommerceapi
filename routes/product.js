const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth")

// Create new Product by Admin Only
router.post("/createproduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addNewProduct(data).then(resultFromController => res.send(resultFromController));
});

// Retrieving All Active Products and disregard the inactive
router.get("/allactive", (req,res) => {
	productController.allActiveProduct().then(resultFromController => res.send(resultFromController));
});

// Retrieving specific Products

router.get("/:productId", (req,res) => {
	productController.specificProduct(req.params).then(resultFromController => res.send(resultFromController));

	});


 //Updating Products for (Admin only)
 router.put("/:productId", auth.verify, (req,res) => {

    productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})


// Archived a specific Product
router.put("/archive/:productId", auth.verify, (req, res) => {
	const data = {
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}
	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;