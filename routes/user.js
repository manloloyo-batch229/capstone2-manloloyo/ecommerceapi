const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth")

// For User Registration
router.post("/register", (req, res) => {
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// This is for User Authentication
router.post("/loginuser", (req, res) => {
	userControllers.logInUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Retrieving user details 

router.post("/details", auth.verify, (req, res) => {
	
	// uses the decode method defined in the auth.js to retrieve user info from request header
	const userData = auth.decode(req.headers.authorization)

	userControllers.getProfile({userId: req.body.id}).then(resultFromController => res.send(resultFromController));
})


/*// Non-Admin User Checkout (Create Order)
router.post("/createorder", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization),
		productId: req.body.productId,
		product: req.body
	}
	userControllers.createOrder(data, req.body, req.params).then(resultFromController => res.send(resultFromController));
})
*/

// for router
module.exports = router;