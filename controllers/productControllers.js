const Product = require("../models/Product.js");

// Create new Product for Admin only
module.exports.addNewProduct = async (data) => {

	if(data.isAdmin){
		
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		});

		return await newProduct.save().then((product, err) => {
			if(err){
				return false	
			}else{
				//return true
				return "Product has been Created!";
			}
		});
	}else{
		return false;
	};
};

// Retrieving All Active Products and disregard the inactive
module.exports.allActiveProduct = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})

}

// Retrieving a specific Product

module.exports.specificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

 //Updating Product for Admin Only

module.exports.updateProduct = async (reqParams, reqBody) => {
	console.log(reqParams);

	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return await Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err) => {
		if(err){
			return false;
		}else{
			return true;
		}
	});
	
};
	
	
// Archive Product (Admin only)
module.exports.archiveProduct = async (data) => {

if(data.isAdmin === true) {
	let hideActiveField = {
		isActive : false
	};

	return await Product.findByIdAndUpdate(data.reqParams.productId, hideActiveField).then((product, err) => {
		if(err){

			return false

		}else{

			return true
		}
	})
	}else {
	return false
	}
};

