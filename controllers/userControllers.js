const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// User Registration
module.exports.registerUser = (reqBody, res) => {
	// will check the email if it is duplicated
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0) {
			return `The Email you entered is already exists.`
		} else {
			let newUser = new User({
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password, 10)
	})
	// Saves the created object to our database
		return newUser.save().then((user, error) => {
			// registration failed
			if(error){
				return false;
			}else{
			// if registration is successful
				return true;
			}
		})
	}
	
	})
}

// For User Authentication if User is log in or not
module.exports.logInUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>  {
		if(result == null){
			return `The entered Email does not exists.`;
		}else {
			const isPasswordIsCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// truthy
			if(isPasswordIsCorrect){
				return {access: auth.createAccessToken(result)}	
			}else{
				return false;
			}
		}
	})
}

// Retrieve User Details
module.exports.getProfile = (data) => {
	console.log(data)
	return User.findById(data.userId).then(result => {
		console.log(result);

		result.password = "";

		return result;
	});
}



/*// User Checkout
module.exports.createOrder = async (data) => {

// For User
	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.newOrders.push({
			productId: data.productId,
			name: product.name

		});

		return user.save().then((user, err) => {
			if(err) {
				return false;
			}else{
				return true;
			};

		});
	});
// For Product
	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.orders.push({userId: data.userId});

		return product.save().then((product, err) => {
			if(err) {
				return false;
			}else{
				return true;
			};
		});
	});
	if(isUserUpdated && isProductUpdated) {
		// if it is successful or true
		return ` User Checkout is successful!`
	}else {
		// error or false
		return `Something Went Wrong Please Try Again.`
	};
}*/


