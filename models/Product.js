// For MongoDB
const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course Name is Required!"]
	},
	description: {
		type: String,
		required: [true, "Product Description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [{
		orderId: {
			type: String,
			required: [true, "UserId is required!"]
		}
	}]
})

module.exports = mongoose.model("Product", productSchema);