const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email:{
		type: String,
		required: [true, "Email is Required in this section!"]
},
	password: {
		type: String,
		required: [true, "Password must be required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	newOrders : [{
	products: [{
			productId: {
				type: String,
				required: [true, "Product Id is Required"]
			},
			quantity: {
				type: Number,
				default: new Number()
			}
		}],
		totalAmount: {
			type: Number,
			required: [true, "Total Amount is Required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		}

	}]

})

module.exports = mongoose.model("User", userSchema);