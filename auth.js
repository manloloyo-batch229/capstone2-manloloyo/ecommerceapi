const jwt = require("jsonwebtoken");

// Token Creation
const secret = "ProductPurchaseOnline"

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {

	// token is retrieved from the request header
	let token = req.headers.authorization;
	console.log(token)

	// token received and is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			
			if(err){
				return res.send({auth: "failed"})		
			
			} else {
				
				next();
			}
		})
	
	// token does not exist
	} else {
		return res.send({auth:"failed"})
	};
}

// Token decryption
module.exports.decode = (token) => {

	// token received and is not undefined
	if(typeof token !== "undefined"){

		// removes the "Bearer" prefix
		token = token.slice(7, token.length);

		// verify method
		return jwt.verify(token, secret, (err, data) => {

			if(err){
				
				return null;
			
			} else {
				 return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		
		return null;
	}
}

